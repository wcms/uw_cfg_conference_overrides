<?php

/**
 * @file
 * uw_cfg_conference_overrides.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_conference_overrides_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_base.
  $overrides["field_base.field_podcast_transcript.field_podcast_transcript"]["DELETED"] = TRUE;
  $overrides["field_base.field_podcast_url.field_podcast_url"]["DELETED"] = TRUE;
  $overrides["field_base.field_related_link.field_related_link"]["DELETED"] = TRUE;
  $overrides["field_base.field_sidebar_content.field_sidebar_content"]["DELETED"] = TRUE;
  $overrides["field_base.field_sidebar_note_content.field_sidebar_note_content"]["DELETED"] = TRUE;
  $overrides["field_base.field_sidebar_note_content_links.field_sidebar_note_content_links"]["DELETED"] = TRUE;
  $overrides["field_base.field_tag.field_tag"]["DELETED"] = TRUE;

  // Exported overrides for: field_instance.
  $overrides["field_instance.node-uw_blog-field_audience.node-uw_blog-field_audience"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_blog-field_podcast_transcript.node-uw_blog-field_podcast_transcript"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_blog-field_podcast_url.node-uw_blog-field_podcast_url"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_blog-field_tag.node-uw_blog-field_tag"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_audience.node-uw_web_page-field_audience"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_related_link.node-uw_web_page-field_related_link"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_sidebar_content.node-uw_web_page-field_sidebar_content"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_sidebar_note_content.node-uw_web_page-field_sidebar_note_content"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_sidebar_note_content_links.node-uw_web_page-field_sidebar_note_content_links"]["DELETED"] = TRUE;

  // Exported overrides for: menu_links.
  $overrides["menu_links.menu-site-management_blog-page-settings:admin/config/system/views_rename.customized"] = 1;
  $overrides["menu_links.menu-site-management_blog-page-settings:admin/config/system/views_rename.hidden"] = 1;
  $overrides["menu_links.menu-site-management_blog-page-settings:admin/config/system/views_rename.menu_links_customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience.customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience.hidden"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience.menu_links_customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags.customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags.hidden"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags.menu_links_customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles.customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles.hidden"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles.menu_links_customized"] = 1;

  // Exported overrides for: views_view.
  $overrides["views_view.uw_blog.display|uw_public_blog_feed|display_options|fields|field_tag|delta_offset"]["DELETED"] = TRUE;
  $overrides["views_view.uw_blog_responsive.display|page|display_options|menu|name"]["DELETED"] = TRUE;
  $overrides["views_view.uw_blog_responsive.display|page|display_options|menu|type"]["DELETED"] = TRUE;
  $overrides["views_view.uw_blog_responsive.display|uw_public_blog_feed|display_options|fields|field_tag|delta_offset"]["DELETED"] = TRUE;
  $overrides["views_view.uw_people_profile_pages.display|page_1|display_options|menu|name"]["DELETED"] = TRUE;
  $overrides["views_view.uw_people_profile_pages.display|page_1|display_options|menu|type"]["DELETED"] = TRUE;
  $overrides["views_view.uwaterloo_services_blog.display|default|display_options|fields|field_podcast_transcript|click_sort_column"]["DELETED"] = TRUE;
  $overrides["views_view.uwaterloo_services_blog.display|default|display_options|fields|field_podcast_transcript|type"]["DELETED"] = TRUE;
  $overrides["views_view.uwaterloo_services_blog.display|default|display_options|fields|field_podcast_url|click_sort_column"]["DELETED"] = TRUE;
  $overrides["views_view.uwaterloo_services_blog.display|default|display_options|fields|field_podcast_url|type"]["DELETED"] = TRUE;

  return $overrides;
}
