<?php

/**
 * @file
 * uw_cfg_conference_overrides.features.inc
 */

/**
 * Implements hook_field_default_field_bases_alter().
 */
function uw_cfg_conference_overrides_field_default_field_bases_alter(&$data) {
  if (isset($data['field_podcast_transcript'])) {
    unset($data['field_podcast_transcript']);
  }
  if (isset($data['field_podcast_url'])) {
    unset($data['field_podcast_url']);
  }
  if (isset($data['field_related_link'])) {
    unset($data['field_related_link']);
  }
  if (isset($data['field_sidebar_content'])) {
    unset($data['field_sidebar_content']);
  }
  if (isset($data['field_sidebar_note_content'])) {
    unset($data['field_sidebar_note_content']);
  }
  if (isset($data['field_sidebar_note_content_links'])) {
    unset($data['field_sidebar_note_content_links']);
  }
  if (isset($data['field_tag'])) {
    unset($data['field_tag']);
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_cfg_conference_overrides_field_default_field_instances_alter(&$data) {
  if (isset($data['node-uw_blog-field_audience'])) {
    unset($data['node-uw_blog-field_audience']);
  }
  if (isset($data['node-uw_blog-field_podcast_transcript'])) {
    unset($data['node-uw_blog-field_podcast_transcript']);
  }
  if (isset($data['node-uw_blog-field_podcast_url'])) {
    unset($data['node-uw_blog-field_podcast_url']);
  }
  if (isset($data['node-uw_blog-field_tag'])) {
    unset($data['node-uw_blog-field_tag']);
  }
  if (isset($data['node-uw_web_page-field_audience'])) {
    unset($data['node-uw_web_page-field_audience']);
  }
  if (isset($data['node-uw_web_page-field_related_link'])) {
    unset($data['node-uw_web_page-field_related_link']);
  }
  if (isset($data['node-uw_web_page-field_sidebar_content'])) {
    unset($data['node-uw_web_page-field_sidebar_content']);
  }
  if (isset($data['node-uw_web_page-field_sidebar_note_content'])) {
    unset($data['node-uw_web_page-field_sidebar_note_content']);
  }
  if (isset($data['node-uw_web_page-field_sidebar_note_content_links'])) {
    unset($data['node-uw_web_page-field_sidebar_note_content_links']);
  }
}

/**
 * Implements hook_menu_default_menu_links_alter().
 */
function uw_cfg_conference_overrides_menu_default_menu_links_alter(&$data) {
  if (isset($data['menu-site-management_blog-page-settings:admin/config/system/views_rename'])) {
    $data['menu-site-management_blog-page-settings:admin/config/system/views_rename']['customized'] = 1; /* WAS: 0 */
    $data['menu-site-management_blog-page-settings:admin/config/system/views_rename']['hidden'] = 1; /* WAS: 0 */
    $data['menu-site-management_blog-page-settings:admin/config/system/views_rename']['menu_links_customized'] = 1; /* WAS: 0 */
  }
  if (isset($data['menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience'])) {
    $data['menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience']['customized'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience']['hidden'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience']['menu_links_customized'] = 1; /* WAS: 0 */
  }
  if (isset($data['menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags'])) {
    $data['menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags']['customized'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags']['hidden'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags']['menu_links_customized'] = 1; /* WAS: 0 */
  }
  if (isset($data['menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles'])) {
    $data['menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles']['customized'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles']['hidden'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles']['menu_links_customized'] = 1; /* WAS: 0 */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_cfg_conference_overrides_views_default_views_alter(&$data) {
  if (isset($data['uw_blog'])) {
    unset($data['uw_blog']->display['uw_public_blog_feed']->display_options['fields']['field_tag']['delta_offset']);
  }
  if (isset($data['uw_blog_responsive'])) {
    unset($data['uw_blog_responsive']->display['page']->display_options['menu']['name']);
    unset($data['uw_blog_responsive']->display['page']->display_options['menu']['type']);
    unset($data['uw_blog_responsive']->display['uw_public_blog_feed']->display_options['fields']['field_tag']['delta_offset']);
  }
  if (isset($data['uw_people_profile_pages'])) {
    unset($data['uw_people_profile_pages']->display['page_1']->display_options['menu']['name']);
    unset($data['uw_people_profile_pages']->display['page_1']->display_options['menu']['type']);
  }
  if (isset($data['uwaterloo_services_blog'])) {
    unset($data['uwaterloo_services_blog']->display['default']->display_options['fields']['field_podcast_transcript']['click_sort_column']);
    unset($data['uwaterloo_services_blog']->display['default']->display_options['fields']['field_podcast_transcript']['type']);
    unset($data['uwaterloo_services_blog']->display['default']->display_options['fields']['field_podcast_url']['click_sort_column']);
    unset($data['uwaterloo_services_blog']->display['default']->display_options['fields']['field_podcast_url']['type']);
  }
}
